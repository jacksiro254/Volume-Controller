using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;

namespace CDM_Socket_Application
{
    class Program
    {
        static void Main(string[] args)
        {
            //return 0;
            ExecuteClient();//https://www.geeksforgeeks.org/socket-programming-in-c-sharp/
        }

        // ExecuteClient() Method
        static void ExecuteClient()
        {

            try
            {

                // Establish the remote endpoint 
                // for the socket. This example 
                // uses port 11111 on the local 
                // computer.
                IPHostEntry ipHost = Dns.GetHostEntry(Dns.GetHostName());
                IPAddress ipAddr = ipHost.AddressList[0];
                IPEndPoint localEndPoint = new IPEndPoint(ipAddr, 20201);

                // Creation TCP/IP Socket using 
                // Socket Class Costructor
                Socket sender = new Socket(ipAddr.AddressFamily,
                           SocketType.Stream, ProtocolType.Tcp);

                try
                {

                    // Connect Socket to the remote 
                    // endpoint using method Connect()
                    sender.Connect(localEndPoint);

                    // We print EndPoint information 
                    // that we are connected
                    Console.WriteLine("Socket connected to -> {0} ",
                                  sender.RemoteEndPoint.ToString());



                    string mac = "";
                    string ack = "";


                    foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
                    {

                        if (nic.OperationalStatus == OperationalStatus.Up && (!nic.Description.Contains("Virtual") && !nic.Description.Contains("Pseudo")))
                        {
                            if (nic.GetPhysicalAddress().ToString() != "")
                            {
                                mac = nic.GetPhysicalAddress().ToString();
                            }
                        }
                    }


                    StringBuilder message = new StringBuilder("<CCP MsgID=\"100\" MsgName=\"AuthoriseReq\" SeqNo=\"1\">");
                    message.Append("<body>");
                    message.Append("<ClientID>1</ClientID>");
                    message.Append("<MACAddress>" + mac + "</MACAddress>");
                    message.Append("<ClientType>UI</ClientType>");
                    message.Append("</body>");
                    message.Append("</CCP>");


                    Console.WriteLine(mac);

                   // Creation of messagge that
                   // we will send to Server
                   byte[] messageSent = Encoding.ASCII.GetBytes(message.ToString());
                   int byteSent = sender.Send(messageSent);

                    // Data buffer
                   byte[] messageReceived = new byte[1024];



                    // We receive the messagge using 
                    // the method Receive(). This 
                    // method returns number of bytes
                    // received, that we'll use to 
                    // convert them to string
                    int byteRecv = sender.Receive(messageReceived);

                   /* String responseData = System.Text.Encoding.ASCII.GetString(messageReceived, 0, byteRecv);

                    Console.WriteLine("Received: {0}", responseData);*/

                    Console.WriteLine("Message from Server -> {0}",
                          Encoding.ASCII.GetString(messageReceived,
                                                     0, byteRecv));


                    message = new StringBuilder("<CCP MsgID=\"10\" MsgName=\"ACK\" SeqNo=\"1\">");
                    message.Append("<body>");
                    message.Append("</CCP>");

                    ack = ACK(1);

                    messageSent = Encoding.ASCII.GetBytes(message.ToString());
                    byteSent = sender.Send(messageSent);

                    // Data buffer
                    messageReceived = new byte[1024];


                    byteRecv = sender.Receive(messageReceived);
                    Console.WriteLine("Message from Server -> {0}",
                          Encoding.ASCII.GetString(messageReceived,
                                                     0, byteRecv));

                    messageSent = Encoding.ASCII.GetBytes(message.ToString());
                    byteSent = sender.Send(messageSent);

                    // Data buffer
                    messageReceived = new byte[1024];

                    byteRecv = sender.Receive(messageReceived);
                    Console.WriteLine("Message from Server -> {0}",
                          Encoding.ASCII.GetString(messageReceived,
                                                     0, byteRecv));



                    //status request
                    message = new StringBuilder("<CCP MsgID = \"102\" MsgName = \"StatusReq\" SeqNo = \"100001\">");
                    message.Append("<body/>");
                    message.Append("</CCP>");

                    while (true)
                    {

                        Message(message.ToString(), sender);

                        ack = ACK(100001);

                        Message(ack.ToString(), sender);


                    }

                        

                    // Close Socket using 
                    // the method Close()
                    sender.Shutdown(SocketShutdown.Both);
                    sender.Close();
                }

                // Manage of Socket's Exceptions
                catch (ArgumentNullException ane)
                {

                    Console.WriteLine("ArgumentNullException : {0}", ane.ToString());
                }

                catch (SocketException se)
                {

                    Console.WriteLine("SocketException : {0}", se.ToString());
                }

                catch (Exception e)
                {
                    Console.WriteLine("Unexpected exception : {0}", e.ToString());
                }
            }

            catch (Exception e)
            {

                Console.WriteLine(e.ToString());
            }
        }





        static void Message(string message, Socket sender)
        {


            Console.WriteLine("UI \n"+ message);

            byte[] messageSent = Encoding.ASCII.GetBytes(message.ToString());
            int byteSent = sender.Send(messageSent);

            // Data buffer
            byte[] messageReceived = new byte[1024];

            // We receive the messagge using 
            // the method Receive(). This 
            // method returns number of bytes
            // received, that we'll use to 
            // convert them to string
            int byteRecv = sender.Receive(messageReceived);

            Console.WriteLine("Message from Server -> {0}",
                  Encoding.ASCII.GetString(messageReceived,
                                             0, byteRecv));

        }


        static string ACK(int SeqNo)
        {

            StringBuilder message = new StringBuilder("<CCP MsgID=\"10\" MsgName=\"ACK\" SeqNo=\""+SeqNo+"\">");
            message.Append("<body>");
            message.Append("</CCP>");

            return message.ToString();
        }

        static void CheckCDCStatus()
        {

            //Check Controller Status


            //Check Sensor Status


            //Check Bag Status 


            //Check
        }
    }


  

    

}

