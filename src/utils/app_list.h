#ifndef APPLIST_H
#define APPLIST_H

#pragma once

#include <Windows.h>
#include <vector>
#include <string>

class AppList
{
public:
    struct app_info
    {
        std::wstring name;
        DWORD id;
        bool x86;
    };
    typedef std::vector<app_info> apps_t;
    typedef std::vector<std::wstring> filters_t;
private:
    bool PopulateList(bool x86, const filters_t&);
    static bool GetAppInfo(app_info&, const filters_t&, bool x86, bool query_name);
public:
    apps_t apps;

    bool PopulateList();
    bool PopulateList(const filters_t&);
    // OR filter
    static bool GetAppInfo(app_info&, const filters_t& = filters_t(), bool query_name = true);
};

#endif // APPLIST_H
