#pragma once

#ifndef VOLUME_CONTROL_H
#define VOLUME_CONTROL_H

#include <QVariant>

#include <mmdeviceapi.h>
#include "volume_control_native.h"

QT_BEGIN_NAMESPACE

class VolumeController
{
public:
    static float getMasterVolume() {
        return VolumeControlNative::GetMasterVolume();
    }

    static bool getMasterMute() {
        return VolumeControlNative::GetMasterMute();
    }

    static float getApplicationVolume(int processId)
    {
        return VolumeControlNative::GetApplicationVolume(processId);
    }

    static bool getApplicationMute(int processId)
    {
        return VolumeControlNative::GetApplicationMute(processId);
    }

    static void setMasterVolume(float level) {
        VolumeControlNative::SetMasterVolume(level);
    }

    static void setMasterMute(bool mute) {
        VolumeControlNative::SetMasterMute(mute);
    }

    static void setApplicationVolume(int processId, float level)
    {
        VolumeControlNative::SetApplicationVolume(processId, level);
    }

    static void setApplicationMute(int processId, bool mute)
    {
        VolumeControlNative::SetApplicationMute(processId, mute);
    }
};

namespace RemoteVolume {
    class VolumeControlNative: public VolumeController {};
} // namespace RemoteVolume

QT_END_NAMESPACE

#endif // VOLUME_CONTROL_H
