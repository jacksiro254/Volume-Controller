#ifndef APPUTIL_H
#define APPUTIL_H

#include <QDebug>

bool AlertHresult(long hr, QString failmessage);
void assertHR(long hr, QString failmessage);

#define DBG_PRINT qDebug() << __FILE__  ":" << __LINE__

#endif // APPUTIL_H
