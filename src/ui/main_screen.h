#ifndef MAINSCREEN_H
#define MAINSCREEN_H

#include <QMainWindow>
#include <QDebug>
#include <memory>

class MainScreen_internal;

class MainScreen : public QMainWindow
{
    Q_OBJECT

        std::unique_ptr<MainScreen_internal> stuff;
public:
    explicit MainScreen(QWidget* parent = 0);
    virtual ~MainScreen();

signals:

public slots:
};

#endif // MAINSCREEN_H
