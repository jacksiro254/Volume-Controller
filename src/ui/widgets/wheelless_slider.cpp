#include <QWheelEvent>
#include "wheelless_slider.h"

WheellessSlider::WheellessSlider(QWidget *parent) : QSlider(parent)
{
    
}

WheellessSlider::WheellessSlider(Qt::Orientation orientation, QWidget *parent) :
    QSlider(orientation, parent)
{ }

WheellessSlider::~WheellessSlider() { }

void WheellessSlider::wheelEvent(QWheelEvent *event) {
    event->ignore();
}
