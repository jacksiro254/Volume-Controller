#include "math.h"

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFrame>
#include <QPushButton>
#include <QLabel>
#include <QSlider>
#include <QAbstractItemModel>
#include <QComboBox>

#include <Functiondiscoverykeys_devpkey.h>
#include <endpointvolume.h>

#include <utils/app_util.h>
#include <models/abstract_volume.h>
#include "device_mixer.h"
#include "device_mixer.h"
#include "volume_slider.h"
#include "session_mixer_list.h"

COM_SMARTPTR(IPropertyStore);
COM_SMARTPTR(IAudioEndpointVolume);

class DeviceMixer::Internals {
public:
    IMMDevicePtr device;
    QSharedPointer<AbstractVolume> dvm;
    QAbstractItemModel *selectorModel;
    
    QVBoxLayout *vbox;
    QHBoxLayout *headerLayout;
    QVBoxLayout *headerTextLayout;
    
    QWidget *iconWidget;
    
    QComboBox *cbxDeviceName;
    QLabel *lblDeviceName;
    QLabel *lblDeviceDesc;
    QLabel *lblStatus;
    
    QPushButton *btnMute;
    QPushButton *btnLinkChannels;
    
    VolumeSlider *sliders;
    
    void InitHeaderWidgets();
};

DeviceMixer::DeviceMixer(QWidget *parent) : QFrame(parent) {
    stuff = new Internals();
    stuff->selectorModel = nullptr;
    stuff->InitHeaderWidgets();
    this->setLayout(stuff->vbox);
    this->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    connect(stuff->cbxDeviceName, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &DeviceMixer::selectedIndexChanged_internal);
}

DeviceMixer::DeviceMixer(QSharedPointer<AbstractVolume> device, QWidget *parent) :
    DeviceMixer(parent)
{
    setModel(device);
}

DeviceMixer::DeviceMixer(QAbstractItemModel *devices, QWidget *parent) :
    DeviceMixer(parent)
{
    setSelectorModel(devices);
}

DeviceMixer::~DeviceMixer() {
    delete stuff;
}

void DeviceMixer::Internals::InitHeaderWidgets() {
    cbxDeviceName = new QComboBox();
    lblDeviceDesc = new QLabel("(Description)");
    lblDeviceName = new QLabel("(Unknown)");
    
    cbxDeviceName->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Fixed);
    lblDeviceName->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Fixed);
    lblDeviceDesc->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Fixed);
    
    headerTextLayout = new QVBoxLayout();
    headerTextLayout->addWidget(cbxDeviceName);
    cbxDeviceName->hide();
    headerTextLayout->addWidget(lblDeviceName);
    headerTextLayout->addWidget(lblDeviceDesc);
    headerTextLayout->addStretch(1);
    
    QFrame *iconWidgetFrame = new QFrame();
    iconWidgetFrame->setFrameStyle(QFrame::Box);
    iconWidgetFrame->setBackgroundRole(QPalette::Text);
    iconWidgetFrame->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    iconWidgetFrame->setMinimumSize(48,48);
    iconWidgetFrame->resize(48,48);
    iconWidget = iconWidgetFrame;
    
    btnMute = new QPushButton(QString::fromWCharArray(L"\uE198")); // mute icon
    btnMute->setFont(QFont("Segoe UI Symbol"));
    btnMute->setCheckable(true);
    btnLinkChannels = new QPushButton("Link");
    btnLinkChannels->setCheckable(true);
    
    headerLayout = new QHBoxLayout();
    headerLayout->addWidget(iconWidget);
    headerLayout->addLayout(headerTextLayout, 1);
    headerLayout->addWidget(btnLinkChannels, 0, Qt::AlignTop);
    headerLayout->addWidget(btnMute, 0, Qt::AlignTop);
    
    vbox = new QVBoxLayout();
    vbox->addLayout(headerLayout);
}

void DeviceMixer::setModel(QSharedPointer<AbstractVolume> newModel) {
    if (stuff->sliders) {
        stuff->vbox->removeWidget(stuff->sliders);
        delete stuff->sliders;
    }
    
    stuff->dvm.clear();
    stuff->dvm = newModel;
    
    stuff->lblDeviceDesc->setText(stuff->dvm->description());
    stuff->lblDeviceName->setText(stuff->dvm->name());
    
    if(stuff->dvm->currentlyHasVolume()) {
        stuff->sliders = new VolumeSlider(stuff->dvm);
        stuff->vbox->addWidget(stuff->sliders);
        connect(stuff->btnLinkChannels, &QPushButton::toggled, stuff->sliders, &VolumeSlider::linkChannels);
        stuff->btnLinkChannels->setChecked(true);
        connect(stuff->btnMute, &QPushButton::clicked, stuff->dvm.data(), &AbstractVolume::setMuted);
        
        connect(stuff->dvm.data(), &AbstractVolume::muteChanged, this, &DeviceMixer::muteClicked);
    }
}

QSharedPointer<AbstractVolume> DeviceMixer::model() {
    return stuff->dvm;
}

void DeviceMixer::refresh() {
}

void DeviceMixer::muteClicked(bool checked) {
    stuff->dvm->setMuted(checked);
    auto label = checked ? L"\uE198" : L"\uE15D";
    stuff->btnMute->setText(QString::fromWCharArray(label));
}

QAbstractItemModel *DeviceMixer::selectorModel() {
    return stuff->selectorModel;
}

void DeviceMixer::setSelectorModel(QAbstractItemModel *items) {
    if(!stuff->selectorModel) {
        stuff->lblDeviceName->hide();
        stuff->cbxDeviceName->show();
    }
    stuff->selectorModel = items;
    stuff->cbxDeviceName->setModel(items);
    stuff->cbxDeviceName->setCurrentIndex(0);
}

void DeviceMixer::clearSelectorModel() {
    stuff->cbxDeviceName->hide();
    stuff->lblDeviceName->show();
    stuff->selectorModel = nullptr;
}

void DeviceMixer::selectedIndexChanged_internal(int index) {
    auto qi = stuff->selectorModel->index(index, 0);
    auto qv = stuff->selectorModel->data(qi, Qt::UserRole);
    auto volumemodel = qv.value<QSharedPointer<AbstractVolume>>();
    
    setModel(volumemodel);
    emit selectedIndexChanged(index);
}
