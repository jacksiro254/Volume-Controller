#ifndef VOLUMESLIDERINTERNAL_H
#define VOLUMESLIDERINTERNAL_H

#include <QObject>
#include <QList>

#include "volume_slider.h"

class AbstractVolume;
class QLabel;
class WheellessSlider;
class QSignalMapper;
class QGridLayout;

class VolumeSlider::Internal : public QObject
{
    Q_OBJECT
    
    friend class VolumeSlider;
    
    QSharedPointer<AbstractVolume> model;
    
    class SliderRow {
        QGridLayout *grid;
        
        QString formatPercentage(float factor);
        
    public:
        QLabel *lblName;
        QLabel *lblValue;
        WheellessSlider *slider;
        
        SliderRow(QString name, QGridLayout *grid, int row);
        ~SliderRow();
        
        void setVisible(bool visible);
        float value();
        void setValue(float val);
    };
    
    QList<SliderRow*> rows;
    QSignalMapper *mapper;
    QGridLayout *grid;
    
    bool linked;
    
    void initChannels();
    void destroyChannels();
    
public:
    explicit Internal(QSharedPointer<AbstractVolume> model, VolumeSlider *parent);
    virtual ~Internal();
    
    void setVolumeModel(QSharedPointer<AbstractVolume> model);
    
signals:
    
public slots:
    void linkChannels(bool linked);
    void hideChannels(bool hidden);
    void setMasterVisible(bool visible);
    
private slots:
    void sliderMoved(int channel);
    void modelUpdated();
};

#endif // VOLUMESLIDERINTERNAL_H
