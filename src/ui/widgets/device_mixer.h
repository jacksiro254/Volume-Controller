#ifndef DEVICEMIXER_H
#define DEVICEMIXER_H

#include <QWidget>
#include <QFrame>
#include <memory>

#include <mmdeviceapi.h>

#include <models/abstract_volume.h>
#include <core/comstuff.h>

COM_SMARTPTR(IMMDevice);
class QAbstractItemModel;

class DeviceMixer : public QFrame
{
    Q_OBJECT
    
    class Internals;
    
    Internals *stuff;
    
    DeviceMixer(QWidget *parent = 0);
    
public:
    explicit DeviceMixer(QSharedPointer<AbstractVolume> device, QWidget *parent = 0);
    explicit DeviceMixer(QAbstractItemModel *devices, QWidget *parent = 0);
    virtual ~DeviceMixer();
    
    QSharedPointer<AbstractVolume> model();
    void setModel(QSharedPointer<AbstractVolume>);
    
    QAbstractItemModel *selectorModel();
    void setSelectorModel(QAbstractItemModel *items);
    void clearSelectorModel();
    
signals:
    void selectedIndexChanged(int index);
    
private slots:
    void refresh();
    void muteClicked(bool checked);
    void selectedIndexChanged_internal(int index);
};

#endif // DEVICEMIXER_H
