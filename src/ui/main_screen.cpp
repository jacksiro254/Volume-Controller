#include <QTabWidget>
#include <QScrollArea>
#include <stdlib.h>
#include <mmdeviceapi.h>

#include "main_screen.h"
#include "widgets/device_mixer_bank.h"
#include "../models/device_collection.h"
#include "../utils/app_util.h"
#include "../core/comstuff.h"

const CLSID CLSID_MMDeviceEnumerator = __uuidof(MMDeviceEnumerator);
const IID IID_IMMDeviceEnumerator = __uuidof(IMMDeviceEnumerator);

COM_SMARTPTR(IMMDeviceCollection);
COM_SMARTPTR(IMMDeviceEnumerator);

class MainScreen_internal {
public:
    MainScreen* self;
    QTabWidget* tabs;
    DeviceMixerBank* playbackPage;
    DeviceMixerBank* recordingPage;
    QScrollArea* deviceDetailPage;
    QWidget* optionPage;

    IMMDeviceEnumeratorPtr devenum;

    DeviceCollection* playbackDevices;
    DeviceCollection* recordingDevices;

    MainScreen_internal(MainScreen* window);
};

MainScreen_internal::MainScreen_internal(MainScreen* window) {
    deviceDetailPage = new QScrollArea();
    optionPage = new QWidget();

    tabs = new QTabWidget();
    tabs->addTab(deviceDetailPage, "Device Details");
    tabs->addTab(optionPage, "Options");

    window->setCentralWidget(tabs);
}

MainScreen::MainScreen(QWidget* parent) : QMainWindow(parent) {
    this->resize(580, 340);

    stuff = std::make_unique<MainScreen_internal>(this);

    HRESULT hr = CoCreateInstance(
        CLSID_MMDeviceEnumerator, NULL,
        CLSCTX_ALL, IID_IMMDeviceEnumerator,
        (void**)&stuff->devenum);

    assertHR(hr, "Couldn't create IMMDeviceEnumerator (%0)");

    IMMDeviceCollectionPtr col;

    stuff->playbackDevices = new DeviceCollection(stuff->devenum, eRender, DEVICE_STATE_ACTIVE);
    stuff->playbackPage = new DeviceMixerBank(stuff->playbackDevices);
    stuff->tabs->insertTab(0, stuff->playbackPage, "Playback");

    stuff->recordingDevices = new DeviceCollection(stuff->devenum, eCapture, DEVICE_STATE_ACTIVE);
    stuff->recordingPage = new DeviceMixerBank(stuff->recordingDevices);
    stuff->tabs->insertTab(1, stuff->recordingPage, "Recording");

    stuff->tabs->setCurrentIndex(0);
}

MainScreen::~MainScreen() { }
