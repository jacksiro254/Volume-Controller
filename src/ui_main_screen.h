/********************************************************************************
** Form generated from reading UI file 'main_screen.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAIN_SCREEN_H
#define UI_MAIN_SCREEN_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainScreen
{
public:
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout_2;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QGroupBox *GrpController;
    QFormLayout *formLayout_2;
    QFormLayout *formLayout;
    QLineEdit *TxtAppPath;
    QSlider *SldVolume;
    QPushButton *BtnMute;
    QLabel *label;
    QSpacerItem *verticalSpacer_3;
    QSpacerItem *horizontalSpacer_2;
    QSpacerItem *verticalSpacer_2;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainScreen)
    {
        if (MainScreen->objectName().isEmpty())
            MainScreen->setObjectName(QString::fromUtf8("MainScreen"));
        MainScreen->resize(450, 300);
        QFont font;
        font.setFamily(QString::fromUtf8("Trebuchet MS"));
        font.setPointSize(12);
        MainScreen->setFont(font);
        centralwidget = new QWidget(MainScreen);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        verticalLayout_2 = new QVBoxLayout(centralwidget);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        GrpController = new QGroupBox(centralwidget);
        GrpController->setObjectName(QString::fromUtf8("GrpController"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(GrpController->sizePolicy().hasHeightForWidth());
        GrpController->setSizePolicy(sizePolicy);
        GrpController->setMinimumSize(QSize(300, 200));
        formLayout_2 = new QFormLayout(GrpController);
        formLayout_2->setObjectName(QString::fromUtf8("formLayout_2"));
        formLayout = new QFormLayout();
        formLayout->setObjectName(QString::fromUtf8("formLayout"));

        formLayout_2->setLayout(1, QFormLayout::LabelRole, formLayout);

        TxtAppPath = new QLineEdit(GrpController);
        TxtAppPath->setObjectName(QString::fromUtf8("TxtAppPath"));

        formLayout_2->setWidget(1, QFormLayout::FieldRole, TxtAppPath);

        SldVolume = new QSlider(GrpController);
        SldVolume->setObjectName(QString::fromUtf8("SldVolume"));
        SldVolume->setSingleStep(5);
        SldVolume->setOrientation(Qt::Horizontal);
        SldVolume->setInvertedAppearance(false);
        SldVolume->setInvertedControls(false);
        SldVolume->setTickPosition(QSlider::TicksBothSides);
        SldVolume->setTickInterval(10);

        formLayout_2->setWidget(2, QFormLayout::FieldRole, SldVolume);

        BtnMute = new QPushButton(GrpController);
        BtnMute->setObjectName(QString::fromUtf8("BtnMute"));
        sizePolicy.setHeightForWidth(BtnMute->sizePolicy().hasHeightForWidth());
        BtnMute->setSizePolicy(sizePolicy);
        BtnMute->setMinimumSize(QSize(100, 0));

        formLayout_2->setWidget(3, QFormLayout::FieldRole, BtnMute);

        label = new QLabel(GrpController);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Trebuchet MS"));
        font1.setPointSize(10);
        label->setFont(font1);

        formLayout_2->setWidget(0, QFormLayout::FieldRole, label);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        formLayout_2->setItem(4, QFormLayout::FieldRole, verticalSpacer_3);


        horizontalLayout->addWidget(GrpController);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        verticalLayout_2->addLayout(horizontalLayout);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_2);

        MainScreen->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainScreen);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 450, 28));
        MainScreen->setMenuBar(menubar);
        statusbar = new QStatusBar(MainScreen);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainScreen->setStatusBar(statusbar);

        retranslateUi(MainScreen);

        QMetaObject::connectSlotsByName(MainScreen);
    } // setupUi

    void retranslateUi(QMainWindow *MainScreen)
    {
        MainScreen->setWindowTitle(QCoreApplication::translate("MainScreen", "MainScreen", nullptr));
        GrpController->setTitle(QCoreApplication::translate("MainScreen", " Volume Controller: ", nullptr));
        BtnMute->setText(QCoreApplication::translate("MainScreen", "Mute", nullptr));
        label->setText(QCoreApplication::translate("MainScreen", "Path to an App (.exe)", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainScreen: public Ui_MainScreen {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAIN_SCREEN_H
