QT       += core gui widgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    core/session_creation_source.cpp \
    main.cpp \
    models/abstract_volume.cpp \
    models/device_collection.cpp \
    models/device_volume.cpp \
    models/session_volume.cpp \
    ui/main_screen.cpp \
    ui/widgets/device_mixer.cpp \
    ui/widgets/device_mixer_bank.cpp \
    ui/widgets/session_mixer_list.cpp \
    ui/widgets/volume_slider.cpp \
    ui/widgets/volume_slider_internal.cpp \
    ui/widgets/wheelless_slider.cpp \
    utils/app_util.cpp \
    utils/get_resource.cpp

HEADERS += \
    core/comstuff.h \
    core/session_creation_source.h \
    core/speaker_formats.h \
    models/abstract_volume.h \
    models/device_collection.h \
    models/device_volume.h \
    models/session_volume.h \
    ui/main_screen.h \
    ui/widgets/device_mixer.h \
    ui/widgets/device_mixer_bank.h \
    ui/widgets/session_mixer_list.h \
    ui/widgets/volume_slider.h \
    ui/widgets/volume_slider_internal.h \
    ui/widgets/wheelless_slider.h \
    utils/app_list.h \
    utils/app_util.h \
    utils/get_resource.h

FORMS += \
    ui/main_screen.ui

RESOURCES += \
    res/resources.qrc \
    res/resources.qrc

LIBS += \
    -"LC:\Program Files (x86)\Windows Kits\10\Lib\10.0.19041.0\um\x64"\
    -lKernel32 \
    -mmdevapi \
    -lOle32 \
    -lUser32 \
    -lcomsuppw \
    -lShlwapi \
    -lMincore

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
